# Telegram Federation Bans Exports

This repository is where we keep exported fedbans from both [Rose](https://t.me/MissRose_bot),
[Sophie](https://t.me/rSophieBot), and others.

## How does fban exports happen?

Exports happens every week on `exports/MissRose_bot/latest.json` and daily on `exports/MissRose_bot/snapshots/*`.[^1]

## [For Staffs/Everyone] How can I export the fbans in JSON file?

Either you can:

* **Ask Andrei Jiroh to get access to Telegram Team Account.** After logging in, open a chat with Rose or Sophie and do a `/fbanlist json`.
* **Let him do the export and forward it to you.** Due to rate limits imposed in fed exports, we limit it to 15 requests daily.

[^1]: Exports from Sophie are also available in `exports/rSophieBot/latest.json` and `exports/rSophieBot/snapshots/*`.
